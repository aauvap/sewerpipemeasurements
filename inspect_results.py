import numpy as np
import os
import open3d as o3d
import csv
import json
import glob
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
from numba import jit

def consensusDataReader(dataFile):
    with open(dataFile, 'r') as csvFile:
        reader = csv.reader(csvFile, delimiter=';')

        firstLine = True

        data = []
        dataNames = dict()

        for row in reader:

            if firstLine:
                entryNumber = 0
                for entry in row:  
                    dataNames[entry] = entryNumber

                    data.append([])
                    entryNumber += 1

                firstLine = False
                continue

            # For the time being, we only want to process Realsense data
            if 'pico' in row:
                continue

            index = 0

            for entry in row:

                if index == 2 or index >= 5:
                    data[index].append(float(entry))
                else:
                    data[index].append(entry)

                index += 1

    return dataNames, data

def singleEstimateDataReader(dataFile):

    data = []
    dataNames = []

    with open(dataFile, 'r') as csvFile:
        reader = csv.reader(csvFile, delimiter=',')

        firstLine = True

        for row in reader:

            if firstLine:
                for entry in row:  
                    self.dataNames.append(entry)

                    self.data.append([])

                firstLine = False
                continue
            
            # For the time being, we only want to process Realsense data
            if 'pico' in row:
                continue

            index = 0

            for entry in row:

                if index == 2 or index >= 7:
                    self.data[index].append(float(entry))
                else:
                    self.data[index].append(entry)

                index += 1

        
    for i in range(len(self.data)):
        if (len(self.data[i]) > 0 and 
            (type(self.data[i][0]) == float)):
            self.data[i] = np.asarray(self.data[i])


def rotation_matrix_from_vectors(vec1, vec2):
    """ Find the rotation matrix that aligns vec1 to vec2
    :param vec1: A 3d "source" vector
    :param vec2: A 3d "destination" vector
    :return mat: A transform matrix (3x3) which when applied to vec1, aligns it with vec2.
    """
    a, b = (vec1 / np.linalg.norm(vec1)).reshape(3), (vec2 / np.linalg.norm(vec2)).reshape(3)
    v = np.cross(a, b)
    c = np.dot(a, b)
    s = np.linalg.norm(v)
    kmat = np.array([[0, -v[2], v[1]], [v[2], 0, -v[0]], [-v[1], v[0], 0]])
    rotation_matrix = np.eye(3) + kmat + kmat.dot(kmat) * ((1 - c) / (s ** 2))
    return rotation_matrix

def find_folders(path):
    paths = []
    if os.path.exists(path + "/data.pickle") and os.path.exists(path + "/meta.pickle"):
        paths.append(path)

    files = os.listdir(path)
    for f in files:
        if os.path.isdir(path + "/" + f):
            paths += find_folders(path + "/" + f)
    return paths


def createDataLookupTable(startPath):

    paths = find_folders(startPath)

    lookup = dict()

    for path in paths:
        timestamp = os.path.basename(path)

        lookup[timestamp] = path

    with open("dataLookupTable.json", 'w') as f  :
        json.dump(lookup, f)

def getCylinderInfoFromFile(filePath):
    with open(filePath, 'r') as f:
        lines = f.readlines()

        if len(lines) < 2:
            return 0, 0, 0

        # Fetching the radius of the estimated cylinder
        radiiInfo = lines[0].split(':')[1].replace('\n', '')
        radiiEst = float(radiiInfo)



        # Fetching the orientation of the cylinder axis
        orientationInfo = lines[1].split(':')[1].replace('\n', '')
        orientationInfo = orientationInfo.split(',')
        orientationEst = np.zeros((3), np.float32)


        for idx, oCoord in enumerate(orientationInfo):
            orientationEst[idx] = float(oCoord)

        # If the third coordinate of the orientation vector is positive, 
        # flip the vector by multiplication with -1
        if orientationEst[2] > 0:
            orientationEst = orientationEst * -1

        # Fetching "pstar", a value on the cylinder axis
        pStarInfo = lines[2].split(':')[1].replace('\n', '')
        pStarInfo = pStarInfo.split(',')
        pStarEst = np.zeros((3), np.float32)

        for idx, pStarVal in enumerate(pStarInfo):
            pStarEst[idx] = pStarVal       


    return radiiEst, orientationEst, pStarEst

def createMeshCylinder(radiiEst, orientationEst, pStarEst, length):
    meshCylinder = o3d.geometry.TriangleMesh.create_cylinder(radius=radiiEst,
                                    height=length, split=int(length*2))

    # Rotate and translate the cylinder such that it fits on the point cloud
    defaultRotVec = np.array([0, 0, 1], np.float32)
    desiredRotVec = orientationEst

    rotMat = rotation_matrix_from_vectors(defaultRotVec, desiredRotVec)
    meshCylinder.rotate(rotMat, center=(0, 0, 0))

    # The cylinder should start at z=0 and go into the positive z range
    # Thus, we need to calculate the new center to displace it
    # The generic cylinder equation is:
    # X = [Pstar] + k * [orientation]
    # where k is a scalar
    #
    # We want to estimate k when the cylinder is a z = 0 and follow the cylinder
    # axis to half its length (e.g. O3D size) 
    kStart = -(pStarEst[2]/orientationEst[2])
    cylinderCenter = pStarEst + (kStart-length/2.)*orientationEst
    meshCylinder.translate(cylinderCenter)

    cylLines = o3d.geometry.LineSet.create_from_triangle_mesh(meshCylinder)

    return cylLines

def visualizeEstimatedCylinderAndCloud(folderPath, sensor, consensusDiameterEstimate, title, gtDiameter, views, cylEstPrefix=None):
    plyFiles = glob.glob(os.path.join(folderPath, 'data-ply', '*.ply'))

    if len(plyFiles) == 0:
        return

    manualCameraControl = True

    if not manualCameraControl:        
        vis = o3d.visualization.Visualizer()
        vis.create_window()

    fig = plt.figure()
    ax = fig.add_subplot()
    fig.subplots_adjust(top=0.80)

    frameIdx = 0

    for pF in plyFiles:

        if 'transformed' in pF:
            continue

        if sensor not in pF:
            # Only show measurements related to this sensor
            continue

        # Fetch the corresponding color image and show it alongside the point cloud
        imgPath = pF.replace('data-ply', 'data-img').replace('.ply', '.png')

        if os.path.exists(imgPath):
            # Check if corresponding image exists
            img = mpimg.imread(imgPath)
            imgplot = plt.imshow(img)

            plt.show(block=False)
          

        

        # Read information on estimated cylinder
        if cylEstPrefix:
            cylEstFile = pF.replace('.ply', '_' + cylEstPrefix + '.txt')
        else:
            cylEstFile = pF.replace('.ply', '.txt')

        radiiEst, orientationEst, pStarEst = getCylinderInfoFromFile(cylEstFile)

        if radiiEst == 0:
            continue

        # Show estimated diameter and additional info
        titleText = (title + ": " + str(gtDiameter) + " mm - Frame " + str(frameIdx)  + "\n" +
                        "Median est: {:.2f}".format(consensusDiameterEstimate) + 
                        "\nFrame est: {:.2f}".format(radiiEst*2000) + " mm")

        fig.suptitle(titleText)
    
        plt.pause(0.1)


        cylinderLength = 2.5 # 2.5 m
        
        pcd = o3d.io.read_point_cloud(pF)
        meshFrame = o3d.geometry.TriangleMesh.create_coordinate_frame(
                                size=0.1, origin=[0, 0, 0])
        
        cylLines = createMeshCylinder(radiiEst, orientationEst, pStarEst, cylinderLength)
        

        geometry = [pcd, meshFrame, cylLines]

        #vis.clear_geometries()
        print(pF)

        if not manualCameraControl:
            for g in geometry:
                vis.add_geometry(g)

            ctr = vis.get_view_control()

            vis.run()
            vis.destroy_window()

        o3d.visualization.draw_geometries(geometry)

        # for view in views:
        #     ctr.set_zoom(view['trajectory'][0]['zoom'])
        #     ctr.set_up(view['trajectory'][0]['up'])
        #     ctr.set_lookat(view['trajectory'][0]['lookat'])
        #     ctr.set_front(view['trajectory'][0]['front'])

        #     vis.poll_events()
        #     vis.update_renderer()
        #     plt.pause(0.1)

            

        frameIdx += 1


        # TODOOOOO
        # ctr = vis.get_view_control()
        # ctr.set_zoom(..)
        # ctr.set_up(...)
        # ctr.set_lookat()
        # ctr.set_front()
        # ctr.change_field_of_view


        # Show alternative poses of the data --> get data from cameraPositions file (json?)

        
        

    plt.close()        

        

def getViewsFromFile(jsonFile):

    with open(jsonFile, 'r') as f:
        raw = json.load(f)

    return raw

if __name__ == "__main__":

    views = getViewsFromFile("./SewerMeasurement/cameraPositions.txt")

    #createDataLookupTable("U:/DataSetCleaned")

    with open("dataLookupTable.json", 'r') as f  :
        dataLookupTable = json.load(f)


    consensusDataFile = 'U://DataSetCleaned/radiiandmse_0.090000_2.500000-analysis.csv'
    consensusDataNames, consensusData = consensusDataReader(consensusDataFile)

    consensusDiameterEstimates = np.array(consensusData[consensusDataNames['radiiMedian']])*2
    groundTruthDiameter = np.array(consensusData[consensusDataNames['groundTruthRadius']])
    cameraTypes = consensusData[consensusDataNames['cameraType']]
    timestamps = consensusData[consensusDataNames['timestamp']]

    consensusEstError = np.abs(np.array(consensusData[2]) - consensusDiameterEstimates)

    fileIdx = [4,6, 7]
    idx = 0

    fFormat = consensusDataFile.replace('-analysis.csv', '')
    params = fFormat.split('_')[1:]

    if len(params) == 0:
        print("Error parsing file: " + fFormat)

    prefix = '_'.join(params)

    # Already looked at: '06_08_2021_12-08-40', 
                        #'06_08_2021_15-59-15', '06_08_2021_16-00-45' '06_08_2021_12-07-44',
                        #'06_08_2021_15-48-03' '06_08_2021_15-49-37', '06_08_2021_15-58-33', '06_08_2021_16-00-04'
                        # '06_08_2021_12-32-00', '06_08_2021_15-00-53', '06_08_2021_14-44-01', '06_08_2021_14-54-11',
                        # '06_08_2021_13-30-45', '06_08_2021_14-39-43', '06_08_2021_14-40-47',

    # '06_08_2021_15-52-02', '06_08_2021_14-30-35', '06_08_2021_11-49-39'

    investigatedList = ['06_08_2021_15-54-04']



    for tStamp, camType, diaEst, err, gtDia in zip(timestamps,
                                                cameraTypes, 
                                                consensusDiameterEstimates, 
                                                consensusEstError,
                                                groundTruthDiameter):

        dataPath = dataLookupTable[tStamp]

        # if err > 20:
        #     continue
        
        if 'pico' in camType:
            continue

        if tStamp not in investigatedList:
            continue

        # if idx not in fileIdx:
        #     idx += 1
        #     continue
        # else:
        #     idx += 1
        print("Investigating: " + tStamp)
        visualizeEstimatedCylinderAndCloud(dataPath, camType, diaEst, tStamp, gtDia, views, cylEstPrefix=prefix)

    # Plan: 
    # - Crawl the directory in U:/DataSetCleaned to make a dict with correspondences 
    #   between timestamps and corresponding paths
    # - Go to directory and unpickle the RGB files in a "data-RGB" folder if not already done beforehand (use pickle_to_ply.py)
    # - For each RGB-depth frame pair, do:
    # - Visualize RGB image
    # - Visualize point cloud with overlaid cylinder estimates
    # ---- Find coordinates in **.txt 
    # ---- Create "dummy" cylinder in Open3D with estimated radius, length 2.500 m
    # ---- Compute rotation matrix from default cylinder axis (0,0,1) to estimated axis (a_x, a_y, a_z)
    # ---- What to do with pstar? - try to draw to z == 0? (see sheet)
    # 
    # Updated plan:
    # Read data file:
    # Get estimate from folder (e.g. timestamp)
    # Propagate estimate to visualization script
    # Inside visualization script, alternate between different viewpoints of point cloud
    # On image: Overlay:
    # timestamp
    # estimated diameter (overall)
    # Current estimate
    # Inlier %, MSE 
    # -------------------------------------------------------------------------------------------------
    # Even newer plan:
    # From the inspections, we found that most failures are a result of misfitted cylinders to noisy data
    # or even very bad data.
    # To see if we can detect such bad data, we have:
    # a) Run the estimation algorithm over a set of different inlier thresholds
    #    in a range between [0.01 m to 0.25 m] 
    # b) Calculated two new metrics, "perCylinderPct" and "perCirclePct" that are estimates of how "large" 
    #    a portion of the estimated cylinder that is accounted for by the points in the point cloud
    # 
    # The hypothesis is that bad cylinder estimates will have relatively low cylinder/circle percentages, 
    # given that many of the points are outside the estimate. 
    #
    # So, to move on from here, we must:
    # 1) Calculate analysis files of the parameter sweep to investigate which inlier threshold(s)
    #    are the optimal for this particular task. For this particular task, we will use the percentage
    #    of samples classified correctly within +/- 20 mm, based on the median radius estimate
    # 2) Investigate whether our new metrics (cylinder/circle percentages) are good estimators of 
    #    the accuracy of the radius estimate. For this, we can use the "linearRegressionExperiments" file







