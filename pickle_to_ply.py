import numpy as np
import pickle
import argparse
import cv2
import open3d as o3d
import os

from PIL import Image


class PickleToPly:
    def __init__(self, meta):
        self.meta = self.load_meta(meta)
        print("Initialization Complete")

    def load_meta(self, path):
        dat = []
        with open(path, 'rb') as f:
            # data = pickle.load(f)
            try:
                while 1:
                    dat.append(pickle.load(f))
            except EOFError:
                pass
        meta = {}
        for d in dat:
            meta[d[0]] = d[1]
        return meta

    def get_points_from_pixels(self, xyCoords, zCoords, cam, coeffs, scale=1.0, lim=[1, 1, 5]):
        # prepare data and coefficients
        cam = np.array(cam, dtype=np.float32)
        coeffs = np.array(coeffs, dtype=np.float32)
        # Remove values with zero depth
        npPos = xyCoords[0,np.nonzero(zCoords),:]

        # undistort
        undistPos = cv2.undistortPoints(npPos, cam, coeffs)

        scaledZCoords = zCoords[np.nonzero(zCoords)] * scale
        clippedZCoords = np.where((scaledZCoords < -1*lim[2]) | (scaledZCoords > lim[2]), 0, scaledZCoords)

        # Transform and scale
        worldXCoords = undistPos[0,:,0] * clippedZCoords
        worldYCoords = undistPos[0,:,1] * clippedZCoords

        # check limits and return
        clippedXValues = np.where((worldXCoords < -1*lim[0]) | (worldXCoords > lim[0]), np.nan, worldXCoords)
        clippedYValues = np.where((worldYCoords < -1*lim[1]) | (worldYCoords > lim[1]), np.nan, worldYCoords)
        
        # If X coordinates are clipped, so should Y and Z
        clippedYValues = np.where(np.isnan(clippedXValues), 0., clippedYValues)
        clippedZValues = np.where(np.isnan(clippedXValues), 0., clippedZCoords)

        # And likewise for Y coordinates, clip X and Z
        clippedXValues = np.where(np.isnan(clippedYValues), 0., clippedXValues,)
        clippedZValues = np.where(np.isnan(clippedYValues), 0., clippedZValues,)

        # We have already clipped X and Y with respect to Z by multiplying with Z above
        # Now, transform NaNs to zeros
        clippedXValues = np.where(np.isnan(clippedXValues), 0., clippedXValues,)
        clippedYValues = np.where(np.isnan(clippedYValues), 0., clippedYValues,)

        combinedArray = list(zip(clippedXValues, clippedYValues, clippedZValues))

        return combinedArray



    def get_point_from_pixel(self, x, y, z, cam, coeffs, scale=1.0, lim=[1, 1, 5]):
        # prepare data and coefficients
        cam = np.array(cam, dtype=np.float32)
        coeffs = np.array(coeffs, dtype=np.float32)
        pos = [[x, y]]
        npPos = np.asarray(pos, np.float32)
        npPos = np.reshape(npPos, (1, 1, 2))

        # undistort
        undistPos = cv2.undistortPoints(npPos, cam, coeffs)

        # Transform and scale
        worldX = (undistPos[0][0][0] * z) * scale
        worldY = (undistPos[0][0][1] * z) * scale
        worldZ = z * scale

        # check limits and return
        if abs(worldX) <= lim[0] and abs(worldY) <= lim[1] and abs(worldZ) <= lim[2]:
            return [worldX, worldY, worldZ]
        else:
            return [0, 0, 0]

    def project_depth_to_3d(self, im, src_type, maskPath=None):

        if maskPath is not None:
            # preprocessing
            mask = cv2.imread(maskPath, 0)
            im = cv2.bitwise_and(im, im, mask=mask)

            # Flip
            im = np.flip(im)

        if src_type == "realsense":
            ppx = self.meta["realsense"]["Depth"]["ppx"]
            ppy = self.meta["realsense"]["Depth"]["ppy"]
            fx = self.meta["realsense"]["Depth"]["fx"]
            fy = self.meta["realsense"]["Depth"]["fy"]
            coeffs = self.meta["realsense"]["Depth"]["coeffs"]
            points = []
            pointcloud = o3d.geometry.PointCloud()

            # cam Matrix
            cam_mat = np.asarray([
                [fx, 0, ppx],
                [0, fy, ppy],
                [0, 0, 1]], np.float32)

            rows = im.shape[0]
            cols = im.shape[1]

            xyCoords = np.zeros((1, rows*cols, 2), np.float32)
            zCoords = np.zeros(rows*cols, np.float32)

            for x in range(0, cols):
                for y in range(0, rows):            
                    xyCoords[0,x*rows+y,0] = x
                    xyCoords[0,x*rows+y,1] = y
                    zCoords[x*rows+y] = im[y][x]

            points = self.get_points_from_pixels(xyCoords, zCoords, cam_mat, coeffs, scale=0.001)


            pointcloud.points = o3d.utility.Vector3dVector(points)
            return [src_type, pointcloud]

        if src_type == "pico_flexx":
            ppx = self.meta["pico_flexx"]["cx"]
            ppy = self.meta["pico_flexx"]["cy"]
            fx = self.meta["pico_flexx"]["fx"]
            fy = self.meta["pico_flexx"]["fy"]
            coeffs = [self.meta["pico_flexx"]["k1"], self.meta["pico_flexx"]["k2"], self.meta["pico_flexx"]["p1"], self.meta["pico_flexx"]["p2"], self.meta["pico_flexx"]["k3"]]
            points = []

            # cam Matrix
            cam_mat = np.asarray([
                [fx, 0, ppx],
                [0, fy, ppy],
                [0, 0, 1]], np.float32)

            npImg = np.asarray(im, dtype=np.float64)
            flippedImage = np.flip(npImg, axis=None)

            rows = flippedImage.shape[0]
            cols = flippedImage.shape[1]

            xyCoords = np.zeros((1, rows*cols, 2), np.float32)
            zCoords = np.zeros(rows*cols, np.float32)

            for x in range(0, cols):
                for y in range(0, rows):            
                    xyCoords[0,x*rows+y,0] = x
                    xyCoords[0,x*rows+y,1] = y
                    zCoords[x*rows+y] = flippedImage[y][x]

            points = self.get_points_from_pixels(xyCoords, zCoords, cam_mat, coeffs)

            pointcloud = o3d.geometry.PointCloud()
            pointcloud.points = o3d.utility.Vector3dVector(points)
            return [src_type, pointcloud]


def find_folders(path):
    paths = []
    if os.path.exists(path + "/data.pickle") and os.path.exists(path + "/meta.pickle"):
        paths.append(path)

    files = os.listdir(path)
    for f in files:
        if os.path.isdir(path + "/" + f):
            paths += find_folders(path + "/" + f)
    return paths

def convertPickleFilesToPly(rootFolder):
    data_folders = find_folders(rootFolder)
    print(data_folders)
    for f in data_folders:
        print("Folder: {}".format(f))
        with open(f + "/data.pickle", 'rb') as fr:
            # Initialize Sample counter
            idx = 0

            # Create parser
            pcc = PickleToPly(f + "/meta.pickle")

            # Create sub-folder for ply files to reside
            plyFilesDir = os.path.join(f, "data-ply")
            os.makedirs(plyFilesDir, exist_ok=True)

            # Read data
            try:
                while 1:
                    #print(idx)
                    # Load Sample

                    dat = pickle.load(fr)

                    if isinstance(dat, list) and isinstance(dat[0], str):                    
                        # Catch first version of recording format

                        # Process depth images
                        if any("realsense" in mdat for mdat in dat[1]):  # Check for realsense data
                            # Load realsense data
                            rs = dat[1]["realsense"]

                            # project points
                            # Check that we get the right realsense depth data
                            rsDepthImg = rs[0]
                            assert len(rsDepthImg.shape) == 2

                            src, pc = pcc.project_depth_to_3d(rsDepthImg, "realsense")

                            # Save individual pointcloud
                            o3d.io.write_point_cloud(os.path.join(plyFilesDir,"sample_{0:04}_realsense.ply".format(idx) ), pc)
        
                        if any("pico_flexx" in mdat for mdat in dat[1]):  # Check for pico flexx data
                            # Load pico flexx data
                            pico = dat[1]["pico_flexx"]

                            # project points
                            src, pc = pcc.project_depth_to_3d(pico, "pico_flexx")

                            # Save individual pointcloud
                            o3d.io.write_point_cloud(os.path.join(plyFilesDir,"sample_{0:04}_pico_flexx.ply".format(idx)), pc)
                    elif isinstance(dat, list) and isinstance(dat[0], list):
                        # Catch second version of recording format
                        for sensorRecording in dat:
                            if 'realsense' in sensorRecording[0]:
                                # project points
                                # Check that we get the right realsense depth data
                                rsDepthImg = sensorRecording[1][2]
                                assert len(rsDepthImg.shape) == 2

                                src, pc = pcc.project_depth_to_3d(rsDepthImg, "realsense")

                                # Save individual pointcloud
                                o3d.io.write_point_cloud(os.path.join(plyFilesDir,"sample_{0:04}_realsense.ply".format(idx) ), pc)
                            if 'pico_flexx' in sensorRecording[0]:
                                pico = sensorRecording[1][1]

                                # project points
                                src, pc = pcc.project_depth_to_3d(pico, "pico_flexx")

                                # Save individual pointcloud
                                o3d.io.write_point_cloud(os.path.join(plyFilesDir,"sample_{0:04}_pico_flexx.ply".format(idx)), pc)

                    # increase sample counter
                    idx += 1
            except EOFError:
                print("Reached the end of data file")
                pass

def convertPickleFilesToImages(rootFolder):
    data_folders = find_folders(rootFolder)
    print(data_folders)
    for f in data_folders:
        print("Folder: {}".format(f))
        with open(f + "/data.pickle", 'rb') as fr:
            # Initialize Sample counter
            idx = 0

            # Create parser
            pcc = PickleToPly(f + "/meta.pickle")

            # Create sub-folder for ply files to reside
            imgFilesDir = os.path.join(f, "data-img")
            os.makedirs(imgFilesDir, exist_ok=True)

            # Read data
            try:
                while 1:
                    #print(idx)
                    # Load Sample

                    dat = pickle.load(fr)

                    if isinstance(dat, list) and isinstance(dat[0], str):                    
                        # Catch first version of recording format

                        # Process depth images
                        if any("realsense" in mdat for mdat in dat[1]):  # Check for realsense data
                            print("To be implemented")
                            assert False
                        
                    elif isinstance(dat, list) and isinstance(dat[0], list):
                        # Catch second version of recording format
                        for sensorRecording in dat:
                            if 'realsense' in sensorRecording[0]:
                                # project points
                                # Check that we get the right realsense depth data
                                rsColorImg = sensorRecording[1][1]
                                assert len(rsColorImg.shape) == 3

                                # Save image to file
                                im = Image.fromarray(rsColorImg)
                                im.save(os.path.join(imgFilesDir,"sample_{0:04}_realsense.png".format(idx)))

                    # increase sample counter
                    idx += 1
            except EOFError:
                print("Reached the end of data file")
                pass

parser = argparse.ArgumentParser(description='Convert sensor recordings from bundled pickle files to seperate .ply files, ' +
                                            'one file per sensor per time stamp.')
parser.add_argument('--rootFolder', dest='rootFolder', help='Path to the folder where the search for pickle files will begin.' +
                    ' Folders inside rootFolder will also be investigated to check if they contain sensor recordings. If so, ' +
                    ' these recordings will also be converted to .ply files', default= "U:/DataSetCleaned")

args = parser.parse_args()

if args.rootFolder is not None:
    convertPickleFilesToPly(args.rootFolder)
    convertPickleFilesToImages(args.rootFolder)
