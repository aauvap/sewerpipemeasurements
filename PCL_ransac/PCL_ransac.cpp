/*

	RANSAC program, finds all ply files in the DATASETFOLDER, runs RANSAC, and outputs results to OUTPUTFILENAME
	
	MAX_Z_DISTANCE: any points with z > MAX_Z_DISTANCE is cropped before RANSAC
	INLIER_DISTANCE: distance used to decide if a point is an inlier or not 
		0.02, 0.05, 0.1 are tried values, 0.05 is best for our data)
	MIN_RADIUS_LIMIT, MAX_RADIUS_LIMIT: minimum and maximum values for the cylinder radius estimated by RANSAC
	METHOD_TYPE: which method to use for fitting a cylinder. RANSAC/PROSAC seems to have same performance,
		but PROSAC runs 2-3 times faster. PROSAC's equivalence should be tested further

	runtime arguments:
	--transform 	outputs ply files for each cloud, which is transformed to lie along the z-axis, 
		based on RANSAC's transform for the cylinder.
	--gui	shows pointcloud and cylinder while running. Updates slowly, but may be useful/interesting

*/

#define DATASETFOLDER "Data"
#define OUTPUTFILENAME "/radiiandmse"
#define MIN_RADIUS_LIMIT 0.05
#define MAX_RADIUS_LIMIT 0.6
#define METHOD_TYPE pcl::SAC_RANSAC //alternatively pcl::SAC_PROSAC

#include <string>
#include <iostream>
#include <fstream>
#include <vector>
#include <cmath>
#include <chrono>
#include <boost/filesystem.hpp>
#include <boost/thread/thread.hpp>
#include <tuple>
#include <algorithm>
#include <iomanip>
#include <iostream>
#include <thread>
#include <mutex>

#include <pcl/common/common.h>
#include <pcl/pcl_macros.h>
#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/features/normal_3d.h>
#include <pcl/io/ply_io.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/ModelCoefficients.h>
#include <pcl/point_types.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/features/normal_3d_omp.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/filters/voxel_grid.h>

#include <Eigen/Geometry>

namespace fs = boost::filesystem;
typedef pcl::PointXYZ PointT;
typedef std::tuple<float, float, pcl::PointCloud<PointT>::Ptr, Eigen::Vector3f, Eigen::Vector3f, float, int> LongTuple;

// new point type for PCL, in order to load doubles from PLY files
#define PCL_NO_PRECOMPILE
struct MyPointXYZ
{
	double x;
	double y;
	double z;
	float padding;

	PCL_MAKE_ALIGNED_OPERATOR_NEW // make sure our new allocators are aligned
} EIGEN_ALIGN16;

POINT_CLOUD_REGISTER_POINT_STRUCT(MyPointXYZ, // here we assume a XYZ + "test" (as fields)
								  (double, x, x)(double, y, y)(double, z, z))

pcl::visualization::PCLVisualizer *viewer;

// Run RANSAC on a single point cloud and return results
LongTuple calcRadius(pcl::PointCloud<PointT>::Ptr cloud, double inlier_distance, bool no_gui, bool no_transform)
{
	pcl::NormalEstimationOMP<PointT, pcl::Normal> ne;
	pcl::SACSegmentationFromNormals<PointT, pcl::Normal> seg;
	pcl::ExtractIndices<PointT> extract;
	pcl::search::KdTree<PointT>::Ptr tree(new pcl::search::KdTree<PointT>());

	pcl::PointCloud<pcl::Normal>::Ptr cloud_normals(new pcl::PointCloud<pcl::Normal>);
	pcl::ModelCoefficients::Ptr coefficients_cylinder(new pcl::ModelCoefficients);
	pcl::PointIndices::Ptr inliers_cylinder(new pcl::PointIndices);

	pcl::PointCloud<PointT>::Ptr cropped_cloud(new pcl::PointCloud<PointT>);

	pcl::PointCloud<PointT>::Ptr transformedCloud(new pcl::PointCloud<PointT>);

	ne.setSearchMethod(tree);
	ne.setInputCloud(cloud);
	ne.setKSearch(50);
	ne.compute(*cloud_normals);

	// Create the segmentation object for cylinder segmentation and set all the parameters
	seg.setModelType(pcl::SACMODEL_CYLINDER);
	seg.setMethodType(METHOD_TYPE);
	seg.setOptimizeCoefficients(true);
	seg.setNormalDistanceWeight(0.1);
	seg.setMaxIterations(10000);
	seg.setDistanceThreshold(inlier_distance);
	seg.setRadiusLimits(MIN_RADIUS_LIMIT, MAX_RADIUS_LIMIT);
	seg.setInputCloud(cloud);
	seg.setInputNormals(cloud_normals);

	//initial guess for coeffiecients, 0 position, axis in Z direction. don't guess for radius
	coefficients_cylinder->values.push_back(0);
	coefficients_cylinder->values.push_back(0);
	coefficients_cylinder->values.push_back(0);
	coefficients_cylinder->values.push_back(0);
	coefficients_cylinder->values.push_back(0);
	coefficients_cylinder->values.push_back(1.0);

	// SACMODEL_CYLINDER - used to determine cylinder models. The seven coefficients of the cylinder are given by a point on its axis, the axis direction, and a radius, as: [point_on_axis.x point_on_axis.y point_on_axis.z axis_direction.x axis_direction.y axis_direction.z radius]
	// Obtain the cylinder inliers and coefficients
	seg.segment(*inliers_cylinder, *coefficients_cylinder);

	std::cout << "Returning from seg.segment" << std::endl;

	//readying some values, used to calc distance.
	//names should correspond to the paper: https://www.sciencedirect.com/science/article/pii/S0097849314001150#f0005
	float mse = 0;
	Eigen::Vector3f pstar;
	pstar[0] = coefficients_cylinder->values[0];
	pstar[1] = coefficients_cylinder->values[1];
	pstar[2] = coefficients_cylinder->values[2];

	Eigen::Vector3f a;
	a[0] = coefficients_cylinder->values[3];
	a[1] = coefficients_cylinder->values[4];
	a[2] = coefficients_cylinder->values[5];

	float y = -acos(a[2]/sqrt(a[0]*a[0] + a[2]*a[2]));
	float x = acos(a[2]/sqrt(a[2]*a[2] + a[1]*a[1]));

	//used to transform in below for loop
	Eigen::Matrix3f m;
	m = Eigen::AngleAxisf(x, Eigen::Vector3f::UnitX())
		* Eigen::AngleAxisf(y,  Eigen::Vector3f::UnitY())
		* Eigen::AngleAxisf(0, Eigen::Vector3f::UnitZ());

	//calculate MSE using formula from https://www.sciencedirect.com/science/article/pii/S0097849314001150#f0005
	for (auto point : cloud.get()->points)
	{
		Eigen::Vector3f pi;
		pi[0] = point.x;
		pi[1] = point.y;
		pi[2] = point.z;

		//Formel 4
		Eigen::Vector3f pi_m_pstar = pi - pstar; //
		float pi_m_pstar_dot_a = pi_m_pstar.dot(a);
		Eigen::Vector3f pi_m_pstar_dot_a_mult_a = pi_m_pstar_dot_a * a;
		Eigen::Vector3f vec = pi_m_pstar - pi_m_pstar_dot_a_mult_a;

		float norm = vec.norm();
		float deltaDist = norm - coefficients_cylinder->values[6];
		mse += deltaDist * deltaDist;

		//optionally write the transformed cloud as a new ply file
		if (no_transform == false)
		{
			pi = pi - pstar;
			Eigen::Vector3f rotatedPoint = m * pi;
			pcl::PointXYZ newPoint(rotatedPoint[0], rotatedPoint[1], rotatedPoint[2]);
			transformedCloud->push_back(newPoint);
		}
	}
	mse /= (float)cloud->size();

	if (no_gui == false)
	{
		float b = -coefficients_cylinder->values[2]/coefficients_cylinder->values[5];
		for(int i = 0; i<3; i++)
        {
            coefficients_cylinder->values[i] = coefficients_cylinder->values[i] + b*coefficients_cylinder->values[3+i];
            if(coefficients_cylinder->values[5] < 0)
                coefficients_cylinder->values[3+i] *= -1;
        }
		viewer->removeShape("cylinder");
		viewer->addCylinder(*coefficients_cylinder, "cylinder");
		if(!viewer->updatePointCloud<PointT>(cloud, "sample cloud"))
			viewer->addPointCloud<PointT>(cloud, "sample cloud");
		viewer->spinOnce(1);
	}
	return LongTuple(abs(coefficients_cylinder->values[6]), mse, transformedCloud, pstar, a, (float)inliers_cylinder->indices.size() / (float)cloud->size(),  inliers_cylinder->indices.size() );
}

//Corrected sample standard deviation
float std_dev(std::vector<float> data)
{
	float mean = 0.0;
	for (int i = 0; i < data.size(); i++)
		mean += data[i] / (float)data.size();

	float sumMmeanSq = 0.0;
	for (int i = 0; i < data.size(); i++)
		sumMmeanSq += pow(data[i] - mean, 2);

	float std_dev = sqrt((1.0 / (float)(data.size() - 1)) * sumMmeanSq);
	return std_dev;
}

//crawls path, and finds paths to all ply files
void directory_crawler(fs::path the_absolute_path, std::vector<fs::path> *output_list)
{

	if (fs::is_directory(the_absolute_path))
	{
		fs::directory_iterator end_iter;
		for (fs::directory_iterator dir_itr(the_absolute_path); dir_itr != end_iter; ++dir_itr)
		{
			try
			{
				if (fs::is_directory(dir_itr->status()))
				{
					//std::cout << "[directory]" << dir_itr->path() << "\n";
					directory_crawler(dir_itr->path(), output_list);
				}
				else if (fs::is_regular_file(dir_itr->status()))
				{
					if (dir_itr->path().extension() == ".ply")
					{
						//std::cout << dir_itr->path() << "\n";
						bool is_transformed = dir_itr->path().filename().string().find("_transformed.ply") != std::string::npos;
						if (!is_transformed) {
							output_list->push_back(fs::path(dir_itr->path()));
						}
						
					}
				}
			}
			catch (const std::exception &ex)
			{
				std::cout << dir_itr->path() << " " << ex.what() << std::endl;
			}
		}
	}
	else
	{
		std::cout << std::endl
				  << "Data isnt a directory" << std::endl;
	}
}

//pull parameters about reading from its path
std::vector<std::string> parse_paramters(fs::path the_file_path)
{
	std::vector<std::string> variables;
	
	for (auto const& fileName : the_file_path) {
		variables.push_back(fileName.string());
	}
	
	int n = 8;
	std::vector<std::string> y(variables.end() - n, variables.end());
	return y;
}

int main(int argc, char *argv[])
{
	/* argument passing, 
		use --gui to show pointcloud and cylinder
		use --transform to calculate and save transformed point clouds
	*/

	if (argc < 5 || argc > 5) {
		cerr << "Usage: PCL_ransac.exe data_folder inlier_distance max_z_distance --gui --no_transform";
		return 0;
	}

	cout << "Parsing arguments" << endl;
	
	std::string data_folder = argv[1];
	double inlier_distance = std::stod(argv[2]);
	double max_z_distance = std::stod(argv[3]);
	bool no_gui = true;
	bool no_transform = true;

	for (int i = 0; i < argc; i++)
	{
		if (std::string("--gui").compare(argv[i]) == 0)
		{
			no_gui = false;
			std::cout << "Gui: " << !no_gui << std::endl;
		}
		else if (std::string("--transform").compare(argv[i]) == 0)
		{
			no_transform = false;
			std::cout << "Transforms: " << !no_transform << std::endl;
		}
	}

	if (no_gui == false) {
		viewer = new pcl::visualization::PCLVisualizer("data viewer");
	}

	std::cout << "PCL Ransac cylinder estimation running with the following parameters:" << std::endl;
	std::cout << "data_folder: " << data_folder << std::endl;
	std::cout << "inlier_distance: " << std::to_string(inlier_distance) << std::endl;
	std::cout << "max_z_distance: " << std::to_string(max_z_distance) << std::endl;
	std::cout << "gui enabled: " << std::to_string(!no_gui) << std::endl;
	std::cout << "Save rectified, transformed point cloud: " << std::to_string(!no_transform) << std::endl;


	//find all ply files in data folder, save their paths
	fs::path data_path(data_folder);
	fs::path full_path = fs::system_complete(data_path);
	std::cout << "Data Path:" << full_path << std::endl;
	std::vector<fs::path> file_paths;
	directory_crawler(full_path, &file_paths);

	pcl::PLYReader reader;
	pcl::PLYWriter plyWriterPointer;
	pcl::PointCloud<MyPointXYZ>::Ptr cloud(new pcl::PointCloud<MyPointXYZ>);

	std::string outFileName = OUTPUTFILENAME "_" + std::to_string(inlier_distance) + "_" + std::to_string(max_z_distance) + ".csv";

	if (fs::is_regular_file(full_path.string() + outFileName))
	{
		std::cout << "Output file exists, please remove it.";
		return 1;
	}

	fstream theFuckingCsvFileNameIsToLong(full_path.string() + outFileName, std::fstream::out);
	theFuckingCsvFileNameIsToLong << "well type, top or bottom, pipe class, measured size, timestamp, cameratype, filename, radii, mse, perInliers, Inliers \n";
	
	if (no_gui == false) {
		viewer->addCoordinateSystem(1.0);
		viewer->initCameraParameters();
		viewer->spinOnce(1);
	}
	std::clock_t c_start = std::clock();
    auto t_start = std::chrono::high_resolution_clock::now();
	int numFrames = 0;

	//main loop - go through all ply file paths, do ransac/prosac, output result to csv file
	for (int j = 0; j < file_paths.size(); j++)
	{
		numFrames++;
		auto fps = (float)numFrames / (std::chrono::duration<double, std::milli>(std::chrono::high_resolution_clock::now()-t_start).count() / 1000.0);
		std::cout << "Progress: " << j << " of " << file_paths.size() << " %" << ((float)j/(float)file_paths.size())*100 << ", fps: " << fps << std::endl; 
		auto filepath = file_paths[j];

		pcl::io::loadPLYFile(filepath.string(), *cloud);
		if (cloud->size() == 0)
		{
			std::cout << "Empty point cloud: " << filepath.string() << std::endl;
			continue;
		}

		std::cout << "Reading and calculating: " << filepath.string();

		pcl::PointCloud<PointT>::Ptr cropped_cloud(new pcl::PointCloud<PointT>);
		for (int i = 0; i < cloud->size(); i++)
		{
			if (cloud->at(i).z < max_z_distance && cloud->at(i).z != 0)
			{
				cropped_cloud->push_back(pcl::PointXYZ(cloud->at(i).x, cloud->at(i).y, cloud->at(i).z));
			}
		}


		std::vector<std::string> variables_for_working_file = parse_paramters(filepath);
		auto radius_mse = calcRadius(cropped_cloud, inlier_distance, no_gui, no_transform);		

		std::string camera_type = "pico";
		if (filepath.filename().string().find("realsense") != std::string::npos)
		{
			camera_type = "realsense";
		}
		theFuckingCsvFileNameIsToLong
			<< variables_for_working_file[1] << "," //Well-type
			<< variables_for_working_file[2] << "," //top or middle
			<< variables_for_working_file[3] << "," //pipe class
			<< variables_for_working_file[4] << "," //measured size
			<< variables_for_working_file[5] << "," //timestamp
			<< camera_type << ","					 //camera type
			<< variables_for_working_file[7] << "," //file name type
			<< std::get<0>(radius_mse)*1000.0 << "," // radius in [mm]
			<< std::get<1>(radius_mse)*1000000.0 << "," // MSE in [mm^2]
			<< std::get<5>(radius_mse) << "," // percent inliers
			<< std::get<6>(radius_mse) << "\n"; // number of inliers

		theFuckingCsvFileNameIsToLong.flush();

		std::cout << std::endl
				  << variables_for_working_file[1] << ", " //Well-type
				  << variables_for_working_file[2] << ", " //top or middle
				  << variables_for_working_file[3] << ", " //pipe class
				  << variables_for_working_file[4] << ", " //measured size
				  << variables_for_working_file[5] << ", " //timestamp
				  << camera_type << ", "				   //camera type
				  << variables_for_working_file[7] << ", " //file name type
				  << std::get<0>(radius_mse) << ", " // radii
				  << std::get<1>(radius_mse) << ", " // mse
				  << std::get<5>(radius_mse) << ", " // perInliers
				  << std::get<6>(radius_mse) << " \n"; // inliers

		

		//write ransac's fitted cylinder to file
		std::string output_file = filepath.string();
		output_file = output_file.substr(0, output_file.size() - 4);
		output_file += "_" + std::to_string(inlier_distance) + "_" + std::to_string(max_z_distance) + ".txt";
		fstream cylinder_file(output_file, std::fstream::out);
		Eigen::Vector3f A = std::get<4>(radius_mse);
		Eigen::Vector3f pstar = std::get<3>(radius_mse);
		cylinder_file << "Radii: " << std::get<0>(radius_mse) 
			<< "\n a: " << A[0] << ", " << A[1] << ", " << A[2] 
			<< "\n pstar: " << pstar[0] << ", " << pstar[1] << ", " << pstar[2];
		cylinder_file.close();


		//write transformed cylinder to file
		if (no_transform == false) {
			std::string filenamePly = filepath.string();
			auto thePointer = std::get<2>(radius_mse);
			filenamePly.insert(filenamePly.length() - 4, "_transformed_" + std::to_string(inlier_distance) + "_" + std::to_string(max_z_distance) + ".");
			std::cout << "Writing: " << filenamePly << std::endl;
			plyWriterPointer.write<pcl::PointXYZ>(filenamePly, *thePointer, true);

			std::string output_file = filenamePly;
			output_file = output_file.substr(0, output_file.size() - 4);
			output_file += "_" + std::to_string(inlier_distance) + "_" + std::to_string(max_z_distance) + ".txt";
			fstream cylinder_file2(output_file, std::fstream::out);
			cylinder_file2 << "Radii: " << std::get<0>(radius_mse);
			cylinder_file2.close();
		}

	}
	theFuckingCsvFileNameIsToLong.close();

	return 0;
}