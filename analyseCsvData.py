import csv
from typing import OrderedDict
from matplotlib.lines import Line2D
import numpy as np
from numpy.core.fromnumeric import mean
import os

import matplotlib.pyplot as plt
from numpy.ma import power
from scipy.sparse import data
import plotter
from scipy.stats import skew

from linearRegressionExperiments import getInlierThresholdFromFileName, parseConsensusDataFile

class PipeMeasurements():
    def __init__(self):
        self.radii = []
        self.mse = []
        self.percentInliers = []
        self.inliers = []

        self.cylPct = []
        self.circlePct = []

        self.cylOrientation = []
        self.numberPoints = []

        self.meta = dict()
        self.filenames = []

    def addFilename(self, filename):
        self.filenames.append(filename)

    def addMeasurement(self, radii, mse, percentInliers, inliers, 
                        cylPct = None, circlePct = None, 
                        cylOrientation = None, numberPoints = None):
        self.radii.append(radii)
        self.mse.append(mse)
        self.percentInliers.append(percentInliers)
        self.inliers.append(inliers)

        if cylPct:
            self.cylPct.append(cylPct)

        if circlePct:
            self.circlePct.append(circlePct)

        if cylOrientation:
            self.cylOrientation.append(cylOrientation)

        if numberPoints:
            self.numberPoints.append(numberPoints)


    def getStatistics(self):
        stats = dict()

        stats['radiiMedian'] = np.median(np.asarray(self.radii))

        weightedSum = 0.
        sumPI = 0.
        for r, pI in zip(self.radii, self.percentInliers):
            weightedSum += r * pI
            sumPI += pI

        if sumPI > 0.:
            stats['radiiWeighted'] = weightedSum / sumPI
        else:
            stats['radiiWeighted'] = 0

        # Inlier stats
        stats['medianPercentInliers'] = np.median(np.asarray(self.percentInliers))
        stats['skewPercentInliers'] = skew(np.asarray(self.percentInliers))
        stats['meanPercentInliers'] = np.mean(np.asarray(self.percentInliers))
        stats['percentInliersStd'] = np.std(np.asarray(self.percentInliers))


        zeroBasedInliers = 0.
        for pI in self.percentInliers:
            zeroBasedInliers += np.power(1. - pI, 2)

        stats['zeroBasedInliers'] = zeroBasedInliers / len(self.percentInliers)

        # MSE stats
        stats['mseMean'] = np.mean(np.asarray(self.mse))
        stats['mseMedian'] = np.median(np.asarray(self.mse))
        stats['mseStd'] = np.std(np.asarray(self.mse))
        stats['mseSkew'] = skew(np.asarray(self.mse))

        # Radii stats
        stats['radiiMean'] = np.mean(np.asarray(self.radii))
        stats['radiiStd'] = np.std(np.asarray(self.radii))
        stats['radiiSkew'] = skew(np.asarray(self.radii))

        # Unwrapped cylinder stats
        if len(self.cylPct) > 0:
            stats['cylPctMean'] = np.mean(np.asarray(self.cylPct))
            stats['cylPctMedian'] = np.median(np.asarray(self.cylPct))
            stats['cylPctStd'] = np.std(np.asarray(self.cylPct))

        # Unwrapped cylinder stats (only looking at circle)
        if len(self.circlePct) > 0:
            stats['circlePctMean'] = np.mean(np.asarray(self.circlePct))
            stats['circlePctMedian'] = np.median(np.asarray(self.circlePct))
            stats['circlePctStd'] = np.std(np.asarray(self.circlePct))

        if len(self.cylOrientation) > 0:
            stats['cylOrientationMean'] = np.mean(np.asarray(self.cylOrientation))
            stats['cylOrientationMedian'] = np.median(np.asarray(self.cylOrientation))
            stats['cylOrientationStd'] = np.std(np.asarray(self.cylOrientation))

        if len(self.numberPoints) > 0:
            stats['numberPointsMean'] = np.mean(np.asarray(self.numberPoints))
            stats['numberPointsMedian'] = np.median(np.asarray(self.numberPoints))
            stats['numberPointsStd'] = np.std(np.asarray(self.numberPoints))


            

        return stats

    def setMetaData(self, meta):
        self.meta = meta

def parseIndividualCsv(individualCsv):
    sets = plotter.getSetsFromFile(individualCsv, skip=False)

    print(sets)

    results = []

    for set in sets:
        cameraMeasurements = {'pico': PipeMeasurements(), 'realsense': PipeMeasurements() } 

        for measurement in set:
            meta = dict()

            meta['wellType'] = measurement[0]
            meta['position'] = measurement[1]
            meta['groundTruthRadius'] = float(measurement[3])
            meta['timestamp'] = measurement[4]
            meta['cameraType'] = measurement[5]
            filename = measurement[6]

            


            radii = float(measurement[7])
            mse = float(measurement[8])
            percentInliers = float(measurement[9])
            inliers = int(measurement[10])
            
            if len(measurement) >= 13:
                cylinderOrientation = float(measurement[11])
                numberPoints = float(measurement[12])

                cameraMeasurements[meta['cameraType']].addMeasurement(radii,
                        mse, 
                        percentInliers, 
                        inliers, 
                        cylOrientation=cylinderOrientation,
                        numberPoints=numberPoints)
            else:
                cameraMeasurements[meta['cameraType']].addMeasurement(radii, mse, percentInliers, inliers)
            
            cameraMeasurements[meta['cameraType']].setMetaData(meta)
            cameraMeasurements[meta['cameraType']].addFilename(filename)

        results.append(cameraMeasurements)

    return results, meta

def calculateConsensusData(individualCsv):
    results, meta = parseIndividualCsv(individualCsv)

    consensusCsv = individualCsv.replace('.csv', '-analysis.csv')


    fieldNames = list(meta.keys()) 
    
    for sensor, res in results[0].items():
        fieldNames += list(res.getStatistics().keys())
        break
        

    with open(consensusCsv, 'w', newline='') as f:

        writer = csv.DictWriter(f, fieldnames=fieldNames)
        writer.writeheader()
        # f.write('wellType;position;groundTruthRadius;timestamp;cameraType;radiiMedian;radiiWeighted;' +
        #         'medianPercentInliers;skewPercentInliers;meanPercentInliers;zeroBasedInliers;' +
        #         'percentInliersStd;mseMean;mseMedian;mseStd;mseSkew;radiiMean;radiiStd;radiiSkew\n')

        for result in results:
            for sensor, res in result.items():
                meta = res.meta
                metrics = res.getStatistics()

                row = {**meta, **metrics}

                writer.writerow(row)

    excelCsv = consensusCsv.replace('.csv', '-excel.csv')



    with open(consensusCsv, 'r') as f:
        filedata = f.read()

    # Replace '.' with ',' to show data in excel:
    filedata = filedata.replace(',', ';')
    filedata = filedata.replace('.', ',')

    with open(excelCsv, 'w') as f:
        f.write(filedata)

def summarizeConsensusDataAcrossThresholds(dataDir):
    # Get all the -analysis data, e.g. the 
    # consensus data across the different 
    # inlier thresholds and create a new file
    # that lists the estimation error for each 
    # folder/data sequence
    #
    # Data structure:
    #               ---------- Estimated error ---------
    # Inlier threshold      0.01    0.02    0.03 ....
    # Folder                ....    ...     ...        

    estimationErrors = dict()

    inlierThresholds = ['Sequence', 'GroundTruthDiameter']

    for dataFile in os.listdir(dataDir):
        if 'excel' in dataFile:
            continue

        if 'inldist' in dataFile:
            continue

        if 'analysis' not in dataFile:
            continue

        inlierThresh = getInlierThresholdFromFileName(dataFile)
        
        if inlierThresh not in inlierThresholds:
            inlierThresholds.append(str(inlierThresh))

        data, estError = parseConsensusDataFile(os.path.join(dataDir, dataFile), sensor='pico')

        folder = data['timestamp']
        groundTruthDiameter = data['groundTruthRadius']        

        for f, gtDia, e in zip(folder, groundTruthDiameter, estError):
            if f not in estimationErrors:
                estimationErrors[f] = dict()

            estimationErrors[f][str(inlierThresh)] = e
            estimationErrors[f]['GroundTruthDiameter'] = gtDia

    fileName = 'estimationErrorAcrossThresholds-pico.csv'

    with open(fileName, 'w', newline='') as csvFile:
        writer = csv.DictWriter(csvFile, fieldnames=inlierThresholds)
        writer.writeheader()

        for key, item in estimationErrors.items():
            item['Sequence'] = key
            writer.writerow(item)

    with open(fileName, 'r') as f:
        d = f.read()

    d = d.replace(',', ';')
    d = d.replace('.', ',')

    with open(fileName.replace('.csv', '-excel.csv'), 'w') as newF:
        newF.write(d)

    print(estimationErrors)


def summarizeConsensusDataAcrossThresholdsAndSensors(dataDir):
    # Get all the -analysis data, e.g. the 
    # consensus data across the different 
    # inlier thresholds, sensor types, and 
    # radius estimators

    # Then calculate the average, median error 
    # and the number of classified pipes within the 
    #  +/- 20 mm threshold
    # Summarize the findings in a single file
    #
    # Final Data structure:
    #               
    # Threshold                     --- 0.01 --- 0.02 ---- ------- 0.24
    # ----          RadiiMean
    # Realsense     RadiiMedian
    # -----         RadiiWeighted
    # ----          RadiiMean
    # Pico          RadiiMedian
    # -----         RadiiWeighted
    
    estimationMetrics = ['radiiMean', 'radiiMedian', 'radiiWeighted']
    sensors = ['pico', 'realsense']

    estimationErrors = dict()

    for sensor in sensors:
        estimationErrors[sensor] = dict()

        for estMet in estimationMetrics:
            estimationErrors[sensor][estMet] = OrderedDict()
        

    inlierThresholds = []

    for dataFile in os.listdir(dataDir):
        if 'excel' in dataFile:
            continue

        if 'inldist' in dataFile:
            continue

        if 'analysis' not in dataFile:
            continue
        
        if 'newMetrics' in dataFile:
            continue

        inlierThresh = getInlierThresholdFromFileName(dataFile)
        
        if inlierThresh not in inlierThresholds:
            inlierThresholds.append(inlierThresh)

        for s in sensors:
            for estMet in estimationMetrics:
                data, estError = parseConsensusDataFile(os.path.join(dataDir, dataFile), 
                                                        sensor=s,
                                                        estimationMetric=estMet)

                estimationErrors[s][estMet][inlierThresh] = (estError, data)
    
    fileName = './SewerMeasurement/estimationErrorAcrossThresholdsAndSensors.csv'

    avgDataForPlots = dict()
    correctThForPlots = dict()

    with open(fileName, 'w', newline='') as csvFile:
        writer = csv.writer(csvFile)

        # Write header manually
        header = ['', 'Threshold']
        header.append(inlierThresholds)
        writer.writerow(header)

        for sensor, metVal in estimationErrors.items():
            for m, thresVal in metVal.items():
                row = [sensor, m]

                plotLabel = sensor + ': ' + m
                avgDataForPlots[plotLabel] = []
                correctThForPlots[plotLabel] = []

                for thresh, errorAndData in thresVal.items():
                    # Get the number of correctly classified pipes within the 
                    # threshold
                    errors, data = errorAndData

                    correctThreshold = (errors < 20).sum() /  len(errors)
                    average = np.mean(errors)
                    row.append("{:.3f} ({:.1f})".format(correctThreshold, average))

                    avgDataForPlots[plotLabel].append(average)
                    correctThForPlots[plotLabel].append(correctThreshold)
                    
        
    # fig, (ax0, ax1) = plt.subplots(nrows=2, ncols=1, sharex=True, figsize=(5,7))

    # linestyles = {'radiiMean':'solid', 'radiiMedian':'dotted', 'radiiWeighted':'dashed'}
    # colors = {'pico': 'tab:blue', 'realsense': 'tab:orange'}

    # for l, dat in avgDataForPlots.items():
    #     linestyle = ''
    #     color = ''

    #     for key, style in linestyles.items():
    #         if key in l:
    #             linestyle = style

    #     for key, clr in colors.items():
    #         if key in l:
    #             color = clr

    #     ax0.plot(np.dot(inlierThresholds, 1000), dat, label=l, linestyle=linestyle, color=color)

    # ax0.set_yscale('log')
    # ax0.legend()
    # ax0.set_ylabel("Estimation error (mm)")
    # #ax0.set_xlabel("RANSAC inlier threshold (mm)")

    # for l, dat in correctThForPlots.items():
    #     linestyle = ''
    #     color = ''

    #     for key, style in linestyles.items():
    #         if key in l:
    #             linestyle = style

    #     for key, clr in colors.items():
    #         if key in l:
    #             color = clr

    #     ax1.plot(np.dot(inlierThresholds, 1000), dat, label=l, linestyle=linestyle, color=color)

    # #ax1.legend()
    # ax1.set_ylabel("Correctly classified pipes")
    # ax1.set_xlabel("RANSAC inlier threshold (mm)")

    # fig.suptitle("Pipe estimation across sensor type and estimation metric")

    # plt.savefig("./SewerMeasurement/Illustrations/estimationErrorAcrossThresholdsAndSensors.pdf", 
    #             bbox_inches='tight')

    # Make another plot highlighting the estimated error at threshold 0.09 compared to the 
    estError, data = estimationErrors['realsense']['radiiMedian'][0.09]

    groundTruthDiameter = data['groundTruthRadius']
    estDiameter = np.dot(data['radiiMedian'],2)

    # Re-calculate the estimated error to get it in non-abs format
    estError = groundTruthDiameter-estDiameter

    fig, (ax0) = plt.subplots(nrows=1, ncols=1, sharex=True)

    # Make a list of colors for each point. A point is 
    # correctly classified if the estimated diameter is within
    # +/- 20 mm of the ground truth diameter -> blue
    # Otherwise, the point is red (or orange if pipe is estimated too large)
    pointColors = []

    for estE in estError:
        if estE >= 20:
            pointColors.append('tab:red')
        elif estE <= -20:
            pointColors.append('tab:orange')
        else:
            pointColors.append('tab:blue')

    # ax0.scatter(groundTruthDiameter, estDiameter, c=pointColors, marker=".")
    # ax0.set_ylabel("Estimated diameter")
    # ax0.set_xlabel("Measured diameter")
    # ax0.add_line(Line2D([20,1120], [0,1100], linewidth=1, linestyle='dashed', color='grey'))
    # ax0.add_line(Line2D([0,1100], [20,1120], linewidth=1, linestyle='dashed', color='grey'))
    # ax0.set_ylim(bottom=0)

    ax0.scatter(groundTruthDiameter, np.abs(estError), c=pointColors, marker=".")
    ax0.set_ylabel("Absolute estimation error (mm)")
    ax0.set_xlabel("Measured diameter (mm)")
    ax0.add_line(Line2D([0,1100], [20,20], linewidth=1, linestyle='dashed', color='grey'))
    ax0.set_yscale('log')
    ax0.set_xlim(left=0, right=1120)

    
    customLegend = [Line2D([0], [0], marker='.', label='$ \epsilon \in \ ]-20,20[$',
                        fillstyle = 'full', markeredgecolor='tab:blue', markerfacecolor='tab:blue', color='w'),
                    Line2D([0], [0], marker='.', label='$ \epsilon \in \ [20,\inf[$',
                        fillstyle = 'full', markeredgecolor='tab:red', markerfacecolor='tab:red', color='w'),
                        Line2D([0], [0], marker='.', label='$ \epsilon \in \ ]\inf,-20]$',
                        fillstyle = 'full', markeredgecolor='tab:orange', markerfacecolor='tab:orange', color='w')]

    ax0.legend(handles=customLegend, loc='lower right')

    plt.savefig("./SewerMeasurement/Illustrations/estimationErrorsForAllPipes.pdf", 
                 bbox_inches='tight')


    plt.show()




def calculateConsensusDataFromFolder(dataPath):
    for path in os.listdir(dataPath):
        if '.csv' not in path:
            continue

        if 'radiiandmse' not in path:
            continue

        if 'analysis' in path:
            continue

        if 'inldist' in path:
            continue

        if 'V2' not in path:
            continue

        # if 'newMetrics' not in path:
        #     # Right now we only focus on processing the "newMetrics"
        #     # file
        #     continue

        combPath = os.path.join(dataPath, path)

        lowerFileSizeThreshold = 1450000

        calculateConsensusData(combPath)

        # if os.path.getsize(combPath) > lowerFileSizeThreshold:
        #     # If the file size is below this threshold, we are not finished 
        #     # calculating the file yet
        #     calculateConsensusData(combPath)


if __name__ == "__main__":
    dataPath = 'U:/DataSetCleaned'
    #calculateConsensusDataFromFolder(dataPath)
    summarizeConsensusDataAcrossThresholdsAndSensors(dataPath)
    #summarizeConsensusDataAcrossThresholds(dataPath)

    





        



        

    





