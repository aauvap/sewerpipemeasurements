import csv
import numpy as np
import os
from numpy.core.fromnumeric import var
from sklearn.linear_model import LinearRegression, LogisticRegression
from sklearn.metrics import confusion_matrix
import matplotlib.pyplot as plt

def parseConsensusDataFile(dataFile, estimationMetric='radiiMedian', sensor='realsense'):
    
    with open(dataFile, 'r') as csvFile:
        sample = csvFile.readline()
        dia = csv.Sniffer().sniff(sample)

        csvFile.seek(0)

        reader = csv.DictReader(csvFile, dialect=dia)

        # Try to detect the dialect of the csv file


        firstLine = True

        data = dict()

        for fieldName in reader.fieldnames:
            data[fieldName] = []

        for row in reader:

            # For the time being, we only want to process data from a particular sensor
            if sensor not in row['cameraType']:
                continue

            for key, entry in row.items():

                try:
                    data[key].append(float(entry))
                except:
                    data[key].append(entry)

                
        for key, entry in data.items():
            if (len(entry) > 0 and 
                (type(entry[0]) == float)):
                data[key] = np.asarray(entry)

        # Calculate the estimation error between ground truth
        # radius and estimated radius
        estError = np.abs(data['groundTruthRadius']-data[estimationMetric]*2)

        # Try different combinations of estimation errors from the RANSAC
        # cylinder estimation algorithm to see if we can use these errors
        # as proxies for the radius estimation error

        # metrics = {'MSEMean': 12, 
        #             'meanPercentInliers': 9, 
        #             'zeroBasedInliers': 10,
        #             'percentInliersStd': 11,
        #             'MSEMedian': 13,
        #             'MSEStd': 14,
        #             'MSESkew': 15,
        #             'medianPercentInliers': 7}

        return data, estError


class PipeMetricExperiments():

    def __init__(self):
        self.data = []
        self.metrics = []

    def _estimateLinReg(self, dataNames, threshold=20, decisionThreshold=20):
            arrays = [self.data[self.metrics[name]] for name in dataNames]
            
            X = np.transpose(np.stack(arrays))
            reg = LinearRegression(normalize=True).fit(X, self.estError)
            print('------------LinReg-------------')
            print(*dataNames, sep=',')
            print("R2-score: {}".format(reg.score(X, self.estError)))
            print("Coeff: {} Intercept: {}".format(reg.coef_, reg.intercept_))

            y = [1 if val < threshold else 0 for val in self.estError]
            yPred = reg.predict(X)
            yPredClass = [1 if val < decisionThreshold else 0 for val in yPred]

            tn, fp, fn, tp = confusion_matrix(y, yPredClass).ravel()
            print(confusion_matrix(y, yPredClass))
            fscore = tp / (tp + 0.5*(fp + fn))
            print("F1-score: {}, FP: {}".format(fscore, fp))

            res = dict()
            res['F1-score'] = fscore
            res['TP'] = tp
            res['TN'] = tn
            res['FN'] = fn
            res['FP'] = fp

            res['estimations'] = dict()

            sequences = self.data[3]

            for sequence, estimate, gtError in zip(sequences, yPred, self.estError):
                res['estimations'][sequence] = (estimate, gtError)

            

            # for i in range(len(yPredClass)):
            #     yGt = y[i]
            #     yP = yPredClass[i]

            #     if yGt == 0 and yP == 1:
            #         print("Score: {}, stamp: {}".format(yPred[i], self.data[3][i]))

            return res


    def _estimateLogReg(self, dataNames, threshold, weight=10):
            arrays = [self.data[self.metrics[name]] for name in dataNames]
            #arrays = [np.zeros_like(self.data[metrics[name]]) for name in self.dataNames]
            
            X = np.transpose(np.stack(arrays))
            y = [1 if val < threshold else 0 for val in self.estError]

            weights = {0: weight, 1:1}

            clf = LogisticRegression(class_weight='balanced').fit(X, y)

            print('------------LogReg-------------')
            print(*dataNames, sep=',')
            print("Mean acc: {}".format(clf.score(X, y)))      
            print("Coeff: {} Intercept: {}".format(clf.coef_, clf.intercept_))

            yPred = clf.predict(X)
            yProb = clf.predict_proba(X)
            tn, fp, fn, tp = confusion_matrix(y, yPred).ravel()
            print(confusion_matrix(y, yPred))
            fscore = tp / (tp + 0.5*(fp + fn))
            print("F1-score: {}, FP: {}".format(fscore, fp))

            # for i in range(len(yPred)):
            #     yGt = y[i]
            #     yP = yPred[i]

            #     if yGt == 0 and yP == 1:
            #         print("Score: {}, stamp: {}".format(yProb[i], self.data[3][i]))


            return fscore, fp, fn

    def runExperimentsOnSingleEstimates(self, dataFile):

        self.data = []
        self.dataNames = []

        self.metrics = dict()

        with open(dataFile, 'r') as csvFile:
            reader = csv.reader(csvFile, delimiter=',')

            firstLine = True

            for row in reader:

                if firstLine:
                    for idx, entry in enumerate(row):  
                        self.metrics[entry] = idx
                        self.data.append([])

                    firstLine = False
                    continue
                
                # For the time being, we only want to process Realsense data
                if 'pico' in row:
                    continue

                index = 0

                for entry in row:

                    if index == 2 or index >= 7:
                        self.data[index].append(float(entry))
                    else:
                        self.data[index].append(entry)

                    index += 1

            
        for i in range(len(self.data)):
            if (len(self.data[i]) > 0 and 
                (type(self.data[i][0]) == float)):
                self.data[i] = np.asarray(self.data[i])

        # Calculate the estimation error between ground truth
        # radius and estimated radius
        self.estError = np.abs(self.data[2]-self.data[7]*2)


        self._estimateLinReg([' mse'])
        self._estimateLinReg(['unwrappedCylinderPct'])
        self._estimateLinReg(['unwrappedCirclePct'])
        # self._estimateLogReg(['mse'], 20)

        # self._estimateLinReg(['perInliers'])
        # self._estimateLogReg(['perInliers'], 20)

        # self._estimateLinReg(['perInliers', 'mse'])
        # self._estimateLogReg(['perInliers', 'mse'], 20)




            


    def runExperimentsOnConsensusEstimates(self, dataFile, metrics):
        
        data, estError, m = parseConsensusDataFile(dataFile)

        self.data = data
        self.estError = estError
        self.metrics = m

        combinedRes = dict()

        self._estimateLinReg(metrics)

        for m in metrics:
            res = self._estimateLinReg([m])
            combinedRes[m] = res
        
        return combinedRes
        # self._estimateLinReg(['meanPercentInliers'])
        # self._estimateLinReg(['zeroBasedInliers'])
        # self._estimateLinReg(['MSEMean', 'zeroBasedInliers'])
        # # self._estimateLinReg(['percentInliersStd'])
        # # self._estimateLinReg(['MSEMedian'])
        # # self._estimateLinReg(['MSEStd'])
        # # self._estimateLinReg(['MSESkew'])
        # # self._estimateLinReg(['medianPercentInliers'])

        # # self._estimateLinReg(['MSEMean', 'medianPercentInliers'])
        # # self._estimateLinReg(['MSEMean', 'percentInliersStd'])
        # # self._estimateLinReg(['MSEMean', 'percentInliersStd', 'zeroBasedInliers'])
        # # self._estimateLinReg(['MSEMean'])
        # # self._estimateLogReg(['MSEMean'], 20)

        # #estimateLogReg(['MSEMean', 'percentInliersStd', 'zeroBasedInliers'], 20)

        # # Plot the top estimators (MSE mean, zeroBasedInliers) compared to the error
        # mseMean = self.data[self.metrics['MSEMean']]
        # zeroBasedInliers = self.data[self.metrics['zeroBasedInliers']]

        # fig = plt.figure()
        # ax = plt.gca()

        # sc = ax.scatter(mseMean, zeroBasedInliers, s=25, c=self.estError, cmap='jet', vmax=40)
        # cbar = plt.colorbar(sc)
        # ax.set_xscale('log')
        # ax.set_yscale('log')

        # plt.show()

        # print(")")



        # fscore = []
        # fp = []
        # fn = []

        # weights = np.arange(0, 20, 0.25)

        # for w in weights:
        #     fscoreW, fpW, fnW = estimateLogReg(['MSEMean'], 20, weight=w)
            
        #     fscore.append(fscoreW)
        #     fp.append(fpW)
        #     fn.append(fnW)

        # fig, ax1 = plt.subplots()
        # ax1.set_xlabel('weight')
        # ax1.set_ylabel('f1-score')
        # ax1.plot(weights, fscore, label='F-score', color='tab:blue')

        # ax2 = ax1.twinx()
        # ax2.set_ylabel('FP/FN')
        # ax2.plot(weights, fp, label='FP', color='tab:orange')
        # ax2.plot(weights, fn, label='FN', color='tab:green')

        # ax1.legend()
        # ax2.legend()

        # fig.tight_layout()
        # plt.title('Log reg')
        # plt.show()

        # print("2")


        # threshold = np.arange(5, 60, 0.5)
        # fscore = []
        # fp = []
        # fn = []

        # for w in threshold:
        #     fscoreW, fpW, fnW = self._estimateLinReg(['MSEMean', 
        #                         'percentInliersStd', 
        #                         'zeroBasedInliers'], 20, decisionThreshold=w)
            
        #     fscore.append(fscoreW)
        #     fp.append(fpW)
        #     fn.append(fnW)

        # fig2, ax21 = plt.subplots()
        # ax21.set_xlabel('weight')
        # ax21.set_ylabel('f1-score')
        # ax21.plot(threshold, fscore, label='F-score', color='tab:blue')

        # ax22 = ax21.twinx()
        # ax22.set_ylabel('FP/FN')
        # ax22.plot(threshold, fp, label='FP', color='tab:orange')
        # ax22.plot(threshold, fn, label='FN', color='tab:green')

        # ax21.legend()
        # ax22.legend()

        # fig2.tight_layout()
        # plt.title('Lin reg')
        # plt.show()

        print("2")
    
def getInlierThresholdFromFileName(filename):
    fFormat = filename.replace('.csv', '')
    params = fFormat.split('_')[1:]

    if len(params) == 0:
        print("Error parsing inlier threshold from: " + fFormat)
        return 0

    inlierThresh = float(params[0])
    return inlierThresh

def collectResultsAcrossInlierThresholds(dataDir):
    results = []

    for dataFile in os.listdir(dataDir):

        if 'excel' in dataFile:
            continue

        if 'analysis' not in dataFile:
            continue

        if 'V2' not in dataFile:
            continue

        if 'inldist' in dataFile:
            continue

        inlierThresh = getInlierThresholdFromFileName(dataFile)
        dataFile = os.path.join(dataDir, dataFile)

        c = PipeMetricExperiments()
        #c.runExperimentsOnSingleEstimates(dataFile)
        combRes = c.runExperimentsOnConsensusEstimates(dataFile, ['medianPercentInliers',
         'skewPercentInliers','meanPercentInliers','percentInliersStd','zeroBasedInliers','mseMean','mseMedian','mseStd','mseSkew','radiiMean','radiiStd','radiiSkew',
         'cylOrientationMean', 'cylOrientationMedian', 'cylOrientationStd',
                            'numberPointsMean', 'numberPointsMedian', 'numberPointsStd'])

        for variable, res in combRes.items():
            res['inlierThresh'] = inlierThresh
            res['Explanatory variable'] = variable

            results.append(res)    

    with open('inlierThreshAnalysis-oldMetricsV2.csv', 'w', newline='') as csvFile:
        fieldNames = ['inlierThresh', 'Explanatory variable', 'F1-score', 'TP', 'TN', 'FN', 'FP']

        writer = csv.DictWriter(csvFile, fieldnames=fieldNames)
        writer.writeheader()

        for row in results:
            row.pop('estimations', None)
            writer.writerow(row)


if __name__ == "__main__":

    collectResultsAcrossInlierThresholds('U:/DataSetCleaned')
    #dataDir = collectResultsAcrossInlierThresholds('U:/DataSetCleaned')

    # Analyse our new metrics, "unwrappedCylinderPct" and "unwrappedCirclePct" # Results: Complete garbage

    p = PipeMetricExperiments()

    investigatedMetrics = ['radiiStd', 'mseMean', 'zeroBasedInliers', 
                            'mseStd', 
                            'cylOrientationMean', 'cylOrientationMedian', 'cylOrientationStd',
                            'numberPointsMean', 'numberPointsMedian', 'numberPointsStd']

    combinedRes = p.runExperimentsOnConsensusEstimates('U:/DataSetCleaned/radiiandmse_0.090000_2.500000-newMetricsV2-analysis.csv', investigatedMetrics)

    with open('estimatorsForError-analysis.csv', 'w', newline='') as csvFile:
        
        fieldNames = ['Sequence', 'gtError'] + list(investigatedMetrics)
        writer = csv.DictWriter(csvFile, fieldnames=fieldNames)

        writer.writeheader()

        # Reverse the order of the results, such that we have a result for each row


        sequenceResults = dict()

        for metric, res in combinedRes.items():
            estimates = res['estimations'] 

            for sequence, estimate in estimates.items():
                if sequence not in sequenceResults:
                    sequenceResults[sequence] = dict()
                    sequenceResults[sequence]['Sequence'] = sequence

                sequenceResults[sequence][metric] = estimate[0]

                if 'gtError' in sequenceResults[sequence]:
                    assert(sequenceResults[sequence]['gtError']  == estimate[1])
                
                sequenceResults[sequence]['gtError'] = estimate[1]
    
        for seq, res in sequenceResults.items():
            writer.writerow(res)
        
        

        
    


    print("Nothing to see here, moving on")

